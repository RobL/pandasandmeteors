"""

    main4.py
    ~~~~~~~

    Plot mass of fallen meteorites

"""

import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

from datetime import datetime

import math

def process():
    """
    Process
    """

    # Pandas reads everything into "dataframes"
    dataframe = pd.read_csv('data/Meteorite_Landings.csv')

    # We can't format it as a date manually either, as it's outside of the pandas
    # date range. So instead we cheat and just extract the year
    dataframe['formatted_year'] = dataframe['year'].map(lambda x: str(x)[6:10])
    dataframe['formatted_year'] = dataframe['formatted_year'].map(lambda x: 0 if x == '' else int(x))

    dataframe['mass (g)'] = dataframe['mass (g)'].map(lambda x: 0 if math.isnan(x) else int(x))

    is_year = dataframe['formatted_year'] > 1900
    is_fell = dataframe['fall'] == "Fell"

    meteorites = dataframe[is_year & is_fell]

    meteorites.groupby(by=['formatted_year'])['mass (g)'].sum().plot(kind='bar')

    # Don't forget this line! Tells us to actually show it
    plt.show()

process()
