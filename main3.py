"""

    main3.py
    ~~~~~~~

    Plot just fallen meteorites

"""

import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

from datetime import datetime

def process():
    """
    Process
    """

    # Pandas reads everything into "dataframes"
    dataframe = pd.read_csv('data/Meteorite_Landings.csv')

    # We can't format it as a date manually either, as it's outside of the pandas
    # date range. So instead we cheat and just extract the year
    dataframe['formatted_year'] = dataframe['year'].map(lambda x: str(x)[6:10])
    dataframe['formatted_year'] = dataframe['formatted_year'].map(lambda x: 0 if x == '' else int(x))

    is_year = dataframe['formatted_year'] > 1900
    is_fell = dataframe['fall'] == "Fell"

    meteorites = dataframe[is_year & is_fell]

    meteorites['formatted_year'].value_counts(sort=False).sort_index(axis=0).plot(kind='bar')

    # Don't forget this line! Tells us to actually show it
    plt.show()

process()
