"""

    countries1.py
    ~~~~~~~

    Get the countries with the highest density of meteorites

"""

import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns


def process():
    """
    Process
    """

    # Pandas reads everything into "dataframes"
    dataframe = pd.read_csv('data/Meteorite_Countries.csv')

    dataframe['Density'] = dataframe['N'] / dataframe['Area']

    print(dataframe.sort_values(by='Density', ascending=False)[:5])

process()
