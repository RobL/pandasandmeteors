"""

    main2.py
    ~~~~~~~

    Plot all meteorite landings by found / fell

"""

import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

from datetime import datetime

def process():
    """
    Process
    """

    # Pandas reads everything into "dataframes"
    dataframe = pd.read_csv('data/Meteorite_Landings.csv')

    dataframe['formatted_year'] = dataframe['year'].map(lambda x: str(x)[6:10])
    dataframe['formatted_year'] = dataframe['formatted_year'].map(lambda x: 0 if x == '' else int(x))

    fall = dataframe['fall'] == "Fell"
    found = dataframe['fall'] == "Found"

    # Limit the year further for the sake of clarity
    is_year = dataframe['formatted_year'] > 1960

    dfFound = dataframe[found & is_year]
    dfFall = dataframe[fall & is_year]

    countFound = dfFound['formatted_year'].value_counts(sort=False).sort_index(axis=0)
    countFall = dfFall['formatted_year'].value_counts(sort=False).sort_index(axis=0)

    # Combine the two series into one data set
    df2 = pd.DataFrame({"fall": countFall, "found": countFound})

    df2.plot(kind='bar')

    # Don't forget this line! Tells us to actually show it
    plt.show()

process()
